(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#5f5f5f" "#ff4b4b" "#a1db00" "#fce94f" "#5fafd7" "#d18aff" "#afd7ff" "#ffffff"])
 '(blink-cursor-mode nil)
 '(debug-on-error t)
 '(elfeed-feeds '("https://www.ahmadoudicko.com/index.xml"))
 '(ess-R-font-lock-keywords
   '((ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:bare-keywords . t)
     (ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:constants . t)
     (ess-fl-keyword:matrix-labels . t)
     (ess-fl-keyword:fun-calls . t)
     (ess-fl-keyword:numbers)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters)
     (ess-fl-keyword:= . t)
     (ess-R-fl-keyword:F&T . t)))
 '(ess-roxy-str "#'")
 '(ess-roxy-template-alist
   '(("description" . ".. content for \\description{} (no empty lines) ..")
     ("details" . ".. content for \\details{} ..")
     ("param" . "")
     ("return" . "")
     ("export" . "")))
 '(inferior-R-font-lock-keywords
   '((ess-S-fl-keyword:prompt . t)
     (ess-R-fl-keyword:messages . t)
     (ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:constants . t)
     (ess-fl-keyword:matrix-labels . t)
     (ess-fl-keyword:fun-calls . t)
     (ess-fl-keyword:numbers)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters)
     (ess-fl-keyword:=)
     (ess-R-fl-keyword:F&T . t)))
 '(inferior-ess-r-font-lock-keywords
   '((ess-S-fl-keyword:prompt . t)
     (ess-R-fl-keyword:messages . t)
     (ess-R-fl-keyword:modifiers . t)
     (ess-R-fl-keyword:fun-defs . t)
     (ess-R-fl-keyword:keywords . t)
     (ess-R-fl-keyword:assign-ops . t)
     (ess-R-fl-keyword:constants . t)
     (ess-fl-keyword:matrix-labels . t)
     (ess-fl-keyword:fun-calls . t)
     (ess-fl-keyword:numbers . t)
     (ess-fl-keyword:operators . t)
     (ess-fl-keyword:delimiters)
     (ess-fl-keyword:= . t)
     (ess-R-fl-keyword:F&T . t)))
 '(org-export-backends '(ascii beamer html latex md odt org))
 '(package-selected-packages
   '(gruvbox-theme perspective ess-view xwwp xwwp-follow-link-ivy grip-mode shades-of-purple shades-of-purple-theme rebecca-theme simple-httpd ob-ess-julia mu4e-marker-icons mu4e-query-fragments async ess-view-data edit-indirect sql-indent comp pass ivy-pass ivy-yasnippet mu4e-views format-all eglot eglot-jl projectile company-stan eldoc-stan flycheck-stan stan-mode stan-snippets ace-window windswap exwm-firefox-core company beacon ergoemacs-mode rust-auto-use rust-mode rust-playground rustic password-store elfeed elfeed-goodies elfeed-org elfeed-protocol ox-gfm ox-hugo ox-leanpub ox-textile ox-tufte julia-snail julia-mode centaur-tabs color-identifiers-mode docker-tramp counsel-tramp cpp-auto-include dpkg-dev-el ivy-rich ess forge magit treemacs-magit bluetooth ranger dired-quick-sort dired-subtree desktop-environment elisp-benchmarks jupyter company-emoji wallpaper sysmon symon frameshot dired-ranger dired-rsync dired-launch ivy-posframe eshell-git-prompt eshell-prompt-extras dashboard dashboard-ls dashboard-project-status eterm-256color vterm vterm-toggle exwm-edit exwm-x atomic-chrome mu4e-conversation mu4e-jump-to-list mu4e-maildirs-extension mu4e-overview mu4e edit-server color-theme julia-repl poly-R poly-rst poly-wdl color-theme-sanityinc-solarized color-theme-sanityinc-tomorrow treemacs-icons-dired auto-yasnippet yasnippet yasnippet-snippets esh-autosuggest eshell-toggle flycheck-grammalecte company-statistics undo-tree auctex ansi package-build shut-up epl git commander f dash s jdee company-reftex company-quickhelp company-auctex company-bibtex company-jedi vlf rjsx-mode ctable pkgbuild-mode apropospriate-theme tangotango-theme alect-themes zerodark-theme company-nginx nginx-mode all-the-icons-ivy dockerfile-mode poly-erb poly-noweb poly-org poly-ruby poly-slim polymode poly-markdown yaml-mode openwith cask tablist js2-mode json-mode org-plus-contrib auto-complete exec-path-from-shell flycheck-gometalinter go-autocomplete go-mode json-navigator graphviz-dot-mode ag company-tern prettier-js tern xref-js2 nyan-mode modern-cpp-font-lock company-rtags flycheck-irony ivy-rtags company-irony irony doom-themes counsel-projectile treemacs-evil treemacs-projectile xterm-color counsel gitlab-ci-mode exwm xelb docker flycheck remark-mode csv-mode material-theme use-package toml-mode no-littering js3-mode indium adaptive-wrap ob-async ob-sql-mode eval-in-repl org-beautify-theme mu4e-alert org-alert all-the-icons-dired org-gcal org-gnome org-jira org-readme skewer-mode tabbar tabbar-ruler emmet-mode confluence org-trello volatile-highlights smart-tabs-mode smart-mode-line-powerline-theme ample-theme ein aggressive-indent neotree all-the-icons ivy-bibtex ivy-gitlab python-environment python-mode python-test python-x slack monroe gitlab swiper smartparens smart-mode-line scala-mode2 powerline ox-reveal ox-nikola ox-ioslide org-eww org-bullets ob-lisp moe-theme mode-icons markdown-mode ido-vertical-mode ido-ubiquitous htmlize hlinum grass-mode flymd eww-lnum ensime elpy cider biblio bbdb auto-complete-auctex auctex-latexmk 0blayout))
 '(polymode-exporter-output-file-format "%s")
 '(safe-local-variable-values '((c-indent-level . 4)))
 '(tool-bar-mode nil)
 '(tramp-backup-directory-alist '(("." . "~/.emacs.d/backups/")))
 '(tramp-syntax 'default nil (tramp))
 '(wallpaper-cycle-directory "~/Pictures/wallpapers2/")
 '(wallpaper-cycle-interval 45)
 '(wallpaper-cycle-single t)
 '(wallpaper-scaling 'scale)
 '(window-divider-default-places t))

;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(default ((t (:family "Operator Mono" :foundry "H&Co" :slant normal :weight normal :height 143 :width normal)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
